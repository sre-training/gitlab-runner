# Allow http
resource "google_compute_firewall" "allow-http" {
  name    = "fw-allow-http"
  network = google_compute_network.sre-vpc.name
  allow {
    protocol = "tcp"
    ports    = ["80","8080","443","8443"]
  }
  
  source_ranges = ["0.0.0.0/0"]
  target_tags = ["http"] 
}
# allow ssh
resource "google_compute_firewall" "allow-ssh" {
  name    = "fw-allow-ssh"
  network = google_compute_network.sre-vpc.name
  allow {
    protocol = "tcp"
    ports    = ["22","2222"]
  }
  source_ranges = ["0.0.0.0/0"]
  target_tags = ["ssh"]
}