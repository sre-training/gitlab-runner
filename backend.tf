terraform {
  cloud {
    organization = "amarolab"

    workspaces {
      name = "gitlab-runner"
    }
  }
}


/*
terraform {
  backend "gcs" {
    bucket  = "gcs-sre-training-amaro"
    prefix  = "lab/sre-training"
  }
}
*/

/*
terraform {
  backend "s3" {
    bucket = "s3sretrainingamarolab"
    key    = "lab/sre-training.tfstate"
    region = "sa-east-1"
  }
}
*/