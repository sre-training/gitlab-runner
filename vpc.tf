resource "google_compute_network" "sre-vpc" {
  name                    = "sre-network"
  auto_create_subnetworks = "true"
}